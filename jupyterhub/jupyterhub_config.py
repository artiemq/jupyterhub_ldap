c.JupyterHub.authenticator_class = 'ldapauthenticator.LDAPAuthenticator'
c.LDAPAuthenticator.bind_dn_template = 'cn={username},dc=example,dc=org'
c.LDAPAuthenticator.server_address = 'openldap'
c.LDAPAuthenticator.use_ssl = False
c.LocalAuthenticator.create_system_users = True
# c.Spawner.notebook_dir = '~/notebooks'
